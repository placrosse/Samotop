# {{crate}} {{version}}

{{readme}}

## Credits

This is a fork of [async-smtp](https://github.com/async-email/async-smtp/releases/tag/v0.3.4) 

## License - {{license}}

<sup>
Licensed under either of <a href="../LICENSE-Apache2">Apache License, Version
2.0</a> or <a href="../LICENSE">MIT license</a> at your option.
</sup>

<br>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in this crate by you, as defined in the Apache-2.0 license, shall
be dual licensed as above, without any additional terms or conditions.
</sub>
