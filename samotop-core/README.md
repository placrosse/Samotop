[![Build Status](https://gitlab.com/BrightOpen/Samotop/badges/develop/pipeline.svg)](https://gitlab.com/BrightOpen/Samotop/commits/master)
![Maintenance](https://img.shields.io/badge/maintenance-activly--developed-brightgreen.svg)

# samotop-core

The domain model of Samotop and core functionality. A base crate for samotop extensions.

License: MIT OR Apache-2.0
